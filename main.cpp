#include <QCoreApplication>

#include "arduinointerface.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ArduinoInterface *ard = new ArduinoInterface();

    return a.exec();
}

#include "arduinointerface.h"

ArduinoInterface::ArduinoInterface(QObject *parent) :
    QObject(parent)
{
    serial = new QSerialPort(this);

    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));

    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));

    openSerialPort();

    file.setFileName("log.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
}

ArduinoInterface::~ArduinoInterface()
{
    closeSerialPort();
    file.close();
}

void ArduinoInterface::openSerialPort()
{
    serial->setPortName("ttyACM0");
    serial->setBaudRate(QSerialPort::Baud9600);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if (serial->open(QIODevice::ReadWrite))
    {
        qDebug() << "Opened";
    }
    else
    {
        qDebug() <<  serial->errorString();
    }
}

void ArduinoInterface::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();
}

void ArduinoInterface::writeData(const QByteArray &data)
{
    serial->write(data);
}
void ArduinoInterface::readData()
{
    QByteArray data = serial->readAll();
    std::string str = data.toStdString();
    writeToFile(str);
}
void ArduinoInterface::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        qDebug() << serial->errorString();
        closeSerialPort();
    }
}

void ArduinoInterface::writeToFile(std::string str)
{
    QTextStream out(&file);
    out << str.c_str();
}

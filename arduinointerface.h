#ifndef ARDUINOINTERFACE_H
#define ARDUINOINTERFACE_H

#include <QObject>
#include <QSerialPort>
//#include <QSerialPortInfo>
#include <QDebug>
#include <QByteArray>
#include <QFile>

#include <iostream>
#include <fstream>

class ArduinoInterface : public QObject
{
    Q_OBJECT
public:

    explicit ArduinoInterface(QObject *parent = 0);
    ~ArduinoInterface();
private slots:
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &data);
    void readData();
    void writeToFile(std::string);
    void handleError(QSerialPort::SerialPortError error);

private:
    void initActionsConnections();
    QList<std::string> list;
private:
    QSerialPort *serial;
    QFile file;
};

#endif // ARDUINOINTERFACE_H

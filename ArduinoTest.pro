#-------------------------------------------------
#
# Project created by QtCreator 2015-07-07T12:51:31
#
#-------------------------------------------------

QT       += core
QT       += network
QT       += serialport
QT       -= gui

TARGET = ArduinoTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    arduinointerface.cpp

HEADERS += \
    arduinointerface.h
